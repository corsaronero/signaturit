

import { InjectionToken } from '@angular/core';

export interface IAppConfig {
    api: any;
}

export let APP_CONFIG = new InjectionToken('app.config');

export const AppConfig: IAppConfig = {

    api: {
        serviceUrl: "http://localhost:3000",
    },

  };