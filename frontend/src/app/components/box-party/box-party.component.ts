import { Component, OnInit,Input, ViewChild, ElementRef } from '@angular/core';
import { IRole } from '../../models/IRole';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-box-party',
  templateUrl: './box-party.component.html',
  styleUrls: ['./box-party.component.scss']
})
export class BoxPartyComponent implements OnInit {

  @Input() roles: IRole[];
  @Input() title:string = '';  
  private contract:string = ''; 
  @Input() i:string; 
  private sum:number = 0;
  modelName:any[] = [];

  constructor(private _dataService: DataService) { }

  ngOnInit() {
  }

  addSign(i, symbol, value){
    if(this.contract.length < 5){
      this.contract += symbol; //concat chart

      if(symbol == 'K' && this.contract.indexOf('V') != -1){
        var count = (this.contract.match(/V/g) || []).length;
        this.sum = this.sum + value - count;
      }
      else if(symbol == 'V' && this.contract.indexOf('K') != -1)
        this.sum = this.sum;
      else
        this.sum = this.sum + value;

      this._dataService.addContract(i, this.contract);
    }
  }

  cleanInputLetter(i){
    let lastChart:any;
    let letterVal:any;

    if(this.sum >= 0){

      lastChart = this.contract[this.contract.length - 1]; //take last chart

      if(lastChart == 'V' && this.contract.indexOf('K') != -1){
        this.sum = this.sum;
      }
      else if(lastChart == 'K' && this.contract.indexOf('V') != -1){
        letterVal = this.roles.find(x => x.symbol === lastChart).value; //find value of chart
        var count = (this.contract.match(/V/g) || []).length;
        this.sum = this.sum - letterVal + count;
      }
      else{
        letterVal = this.roles.find(x => x.symbol === lastChart).value; //find value of chart
        this.sum = this.sum - letterVal; 
      }

      this.contract =  this.contract.slice(0, -1);
      this._dataService.addContract(i, this.contract);
    }
    else if(this.contract.indexOf('V') != -1){
      this.contract =  this.contract.slice(0, -1);
      this._dataService.addContract(i, this.contract);
    }
  }

}
