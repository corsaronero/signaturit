import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoxPartyComponent } from './box-party.component';

describe('BoxPartyComponent', () => {
  let component: BoxPartyComponent;
  let fixture: ComponentFixture<BoxPartyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoxPartyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoxPartyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
