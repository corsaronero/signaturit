import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { IRole } from '../../models/IRole';
import { Observable, Subscription } from 'rxjs';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  roles: IRole[];
  listTemplate:any;
  title1 = "Party 1";
  title2 = "Party 2";
  index1 = 1;
  index2 = 2;
  errorMessage:string;
  errorMessage1:string;
  errorMessage2:string;
  items = {}
  isVisible:boolean = false;

  private subscription: Subscription;
  contract1:string;
  contract2:string;
  winner = [];

  constructor(private _dataService: DataService) { }
  
  ngOnInit() {
    this.subscription = this._dataService.getRoles().subscribe(res => this.roles = res);
    this.subscription = this._dataService.contractObs1.subscribe(res => this.contract1 = res);
    this.subscription = this._dataService.contractObs2.subscribe(res => this.contract2 = res);
  }

  getData(){

      if(this.contract1.length != 0 && this.contract2.length !=0){
        
        this.items = {
          "contract1":this.contract1,
          "contract2":this.contract2
        }
    
        this._dataService.postDataAPI(this.items)
            .subscribe(
                (res: any) => {
                    this.winner.push(res);
                }, // (1)
                (error: any) => { 
                  
                    this.isVisible = true;
                    this.errorMessage = error.error;
                },
                () => {
                  console.log('completed')
                  this.isVisible = false;
                } 
            );
      }
      else{
        this.isVisible = true;
        if(this.contract1.length == 0)
            this.errorMessage = "The input Party 1 it's required"
        else
            this.errorMessage = "The input Party 2 it's required"
      }
  }

  //unsubscribe subscription (Memory Leak)
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
  
}
