import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';

//Components
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { BoxPartyComponent } from './components/box-party/box-party.component';

//Services
import { DataService } from './services/data.service';

// Config
import { APP_CONFIG, AppConfig } from './app_config/app.config';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    BoxPartyComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    DataService,
    { provide: APP_CONFIG, useValue: AppConfig }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
