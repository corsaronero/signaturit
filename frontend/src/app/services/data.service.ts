
import { Injectable,Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, BehaviorSubject, throwError } from 'rxjs';
import { IRole } from '../models/IRole';
import { APP_CONFIG, IAppConfig } from '../app_config/app.config';
import { catchError } from 'rxjs/operators';

@Injectable()
export class DataService {

    public serviceUrl: string;

    private contractSource1 = new BehaviorSubject<string>('');
    contractObs1 = this.contractSource1.asObservable();

    private contractSource2 = new BehaviorSubject<string>('');
    contractObs2 = this.contractSource2.asObservable(); 

    private headers = new HttpHeaders({
        'Content-Type': 'application/json'
    });

    constructor(@Inject(APP_CONFIG) appConfig: IAppConfig, private _http: HttpClient){
        this.serviceUrl = appConfig.api.serviceUrl;
    }

    getRoles(): Observable<IRole[]>{
        return this._http.get<any[]>(this.serviceUrl + '/api/roles')
    }

    postDataAPI(items): Observable<any>{
    return this._http
        .post(this.serviceUrl + '/api/contracts', items, { headers: this.headers })
        .pipe(
            catchError(err => {
                console.log('Handling error locally and rethrowing it...', err);
                return throwError(err);
            })
        )
    }

    addContract(i, item){
        if(i == 1)
            this.contractSource1.next(item);
        else
            this.contractSource2.next(item);
    }

}