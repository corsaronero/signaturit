/* Defines the Role entity */
export interface IRole {
    id: number;
    name: string;
    symbol: string;
    value: number;
}