
const mysqlConnection = require('../config/connection');

var dbProducts ={

    gellRoles:function(callback){
        return mysqlConnection.query("SELECT * FROM roles", callback);
    },

    getValueByLetter:function(letter,callback){
        return mysqlConnection.query("SELECT value FROM roles where symbol=?", [letter], callback);
    }
}

module.exports = dbProducts;