
const mysql = require('mysql');

var mysqlConnection = mysql.createConnection({
    host:"localhost",
    user:"root",
    password:"",
    database:"lobby",
    multipleStatements:true
});

mysqlConnection.connect(function(err){
    if(!err) {
        console.log("Database is connected ... \n\n");  
    } else {
        console.log("Error connecting database ... \n\n", err);  
    }
});

module.exports = mysqlConnection;