
const Joi = require('joi');
const express = require('express');
const bodyParser = require('body-parser');
const mysqlConnection = require('./config/connection');
const dbProducts = require('./models/dbProducts')

//Inizialize Express App
var app = express();

//Use Middlewares
app.use(bodyParser.json()); // parse form data client

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Credentials", "true");
    res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    next();
});

//Import API Routes
app.use(require('./routes/products'));

const contracts = ['KN','NNV'];
var data = [];

app.post('/api/contracts', (req, res) => {

    var items = [];

    const { error } = validateContract(req.body); //result.error
    //Send Error + Staus server
    if(error) return res.status(400).send(error.details[0].message); 

    for (var key in req.body) {

        if (req.body.hasOwnProperty(key)) {
          item = req.body[key];
        
            if (!contracts.includes(item))
                return  res.status(403).send("Contract " + item + " not valid");
            else
                items.push(item);
        }
    }
    
    calculateWinner(items, function(myRenderArray){
        var result = "";

        if(myRenderArray){
            console.log("MyArray", myRenderArray)
            var indexOfMaxValue = myRenderArray.reduce((iMax, x, i, arr) => x > arr[iMax] ? i : iMax, 0);

            if(indexOfMaxValue == 0)
                result = ("Party1 ( " + myRenderArray[0] + " points ) as won to Party2 ( " + myRenderArray[1] + " points )");
            else
                result = ("Party2 ( " + myRenderArray[1] + " points ) as won to Party1 ( " + myRenderArray[0] + " points )");
        }

        res.json(result);
    });
    
});

calculateWinner = function calculateWinner(items, callback){

    var array = [];

    dbProducts.gellRoles(function(err, rows){
        if(data.length == 0){
            for(var i = 0; i<rows.length; i++){
                data.push({
                    'symbol': rows[i].symbol,
                    'value': rows[i].value
                });
            }
        }

        for(let i = 0; i < items.length; i++){
            let sum = 0;
    
            for (let str of items[i]){
                
                for(let k = 0; k < data.length; k++){
                    
                    if(str == data[k].symbol){
                        sum += data[k].value;
                        array[i] = sum; 
                    }
                }
            }
        }
        return callback(array);
    });
}

//Validator if is string max length and required text
function validateContract(contract){
    const schema = {
        contract1: Joi.string().max(3).required(),
        contract2: Joi.string().max(3).required()
    };
    return Joi.validate(contract, schema);
}

// set express to use this port
port = process.env.port || 3000;

app.listen(port, function(){
    console.log("listening to port" + port);
})